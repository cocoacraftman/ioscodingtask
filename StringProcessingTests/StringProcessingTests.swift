//
//  StringProcessingTests.swift
//  StringProcessingTests
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import UIKit
import XCTest

class StringProcessingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test() {
        if let sourceFilePath = NSBundle.mainBundle().pathForResource("extract", ofType: "json") {
            if let data = NSData(contentsOfFile: sourceFilePath) {
                
                if let rootDict: AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: nil) {
                    if let tests = rootDict["tests"] as? NSDictionary {
                        if let mentions = tests["mentions"] as? NSArray {
                            self.testMention(mentions)
                        }
                        
                        if let emoticons = tests["emoticons"] as? NSArray {
                            self.testEmoticon(emoticons)
                        }
                        
                        if let links = tests["links"] as? NSArray {
                            self.testLink(links)
                        }
                        
                        
                    } else {
                        XCTFail("invalid test data")
                        return
                    }
                    
                } else {
                    XCTFail("invalid test data")
                    return
                }
                
            } else {
                XCTFail("no test data")
                return
            }
            
        } else {
            XCTFail("no test data)")
            return
        }
        

    }
    
    
    
    func testMention(mentions: NSArray) {
        for mention in mentions {
            if let testCase = mention as? NSDictionary {
                var text : String = testCase.objectForKey("text") as! String
                var expected : [String] = testCase.objectForKey("expected") as! [String]
                var actual = StringExtractor(_string: text).scanForMetionedUsers()
                XCTAssertEqual(expected, actual!, "success")
            } else {
                XCTFail("mention test case is not valid")
            }
        }
    }
    
    func testEmoticon(mentions: NSArray) {
        for mention in mentions {
            if let testCase = mention as? NSDictionary {
                var text : String = testCase.objectForKey("text") as! String
                var expected : [String] = testCase.objectForKey("expected") as! [String]
                var actual = StringExtractor(_string: text).scanForEmoticons()
                XCTAssertEqual(expected, actual!, "success")
            } else {
                XCTFail("emoticon test case is not valid")
            }
        }
    }
    
    func testLink(links:NSArray) {
        for link in links {
            if let testCase = link as? NSDictionary {
                 var text : String = testCase.objectForKey("text") as! String
                var expectedLink : [String] = testCase.objectForKey("expectedLink") as! [String]
                var expectedTitle : [String] = testCase.objectForKey("expectedTitle") as! [String]
                var actual = StringExtractor(_string: text).scanForURLs()
                if let actualURlEntities = actual {
                    var links = [String]()
                    var titles = [String]()
                    for entity in actualURlEntities {
                        links.append(entity.url)
                        if let title = entity.title {
                            titles.append(title)
                        }
                    }
                    
                    XCTAssertEqual(links, expectedLink, "url success")
                    XCTAssertEqual(titles   , expectedTitle, "title success")

                } else {
                    XCTFail("something went wroing")
                }
            } else {
                XCTFail("link test case is not valid")

            }
        }
    }
    
    
    
//    for (NSDictionary *testCase in mentions) {
//    NSString *text = [testCase objectForKey:@"text"];
//    NSArray *expected = [testCase objectForKey:@"expected"];
//    
//    NSArray *results = [TwitterText mentionsOrListsInText:text];
//    if (results.count == expected.count) {
//    NSUInteger count = results.count;
//    for (NSUInteger i = 0; i < count; i++) {
//    NSString *expectedText = [expected objectAtIndex:i];
//    
//    TwitterTextEntity *entity = [results objectAtIndex:i];
//    NSRange actualRange = entity.range;
//    actualRange.location++;
//    actualRange.length--;
//    NSString *actualText = [text substringWithRange:actualRange];
//    
//    XCTAssertEqualObjects(expectedText, actualText, @"%@", testCase);
//    }
//    } else {
//    XCTFail(@"Matching count is different: %lu != %lu\n%@", expected.count, results.count, testCase);
//    }
//    }
    
}
