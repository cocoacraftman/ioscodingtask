//
//  ResultVC.swift
//  
//
//  Created by Võ Long on 7/5/15.
//
//

import UIKit

class ResultVC: UIViewController {
    // test case string : is passed from RootVC
    var testCase : String!
    // result string after extracting : is passed from RootVC
    var result : String!
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = String(format: "TestCase : %@\n Result : %@\n",self.testCase,self.result)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
