//
//  RegexManager.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import Foundation
class Regex {
    ///
    let regex : NSRegularExpression?
    //Pattern String for RegularExpression Object
    let pattern : String
    /*!
    * @discussion failable init function
    * @param _pattern : String
    * @return Regex instance
    */
    init?(_pattern : String) {
        self.pattern = _pattern
        var error : NSError?
        self.regex = NSRegularExpression(pattern: self.pattern, options: nil, error: &error)
        if (self.regex == nil) || (error != nil) {
            return nil
        }
    }
    /*!
    * @discussion remove special character from string
    * @param _string : String
    * @return String
    */
    func removeSpecialCharacter(_string : String) -> String {
        return self.regex!.stringByReplacingMatchesInString(_string, options: nil, range: NSMakeRange(0, count(_string)), withTemplate: "")
    }
    /*!
    * @discussion extract entity by regex pattern from string
    * @param _string : String
    * @return String
    */
    func extractEntityFromString(_string : String) -> Array<String>? {
        //get list of match result
        var results = self.regex!.matchesInString(_string, options: nil, range: NSMakeRange(0, count(_string)))
        
        var entities : Array<String> = [String]()
        
        for result in results {
            
            if let matchResult  = result as? NSTextCheckingResult {
                let range = matchResult.range
                // get match string
                let matchedText : String = (_string as NSString).substringWithRange(range)
                entities.append(matchedText)
            }
        }
        
        if entities.count > 0 {
            return entities
        } else {
            return nil
        }
        
    }
    /*!
    * @discussion get first matching result from string by Pattern Regex
    * @param _string : String
    * @return String
    */
    func getFirstEntityFromString(_string : String) -> String? {
        
        if let result = self.regex!.firstMatchInString(_string, options: nil, range: NSMakeRange(0, count(_string))) {
            return (_string as NSString).substringWithRange(result.range)
        }
        return nil
        
    }
    
    
    
    
    
    
    
}