//
//  String+Regex.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import Foundation

extension String {
    /*!
    * @discussion extract entity by regex pattern from string
    * @param _regexPattern : String regex Pattern
    * @return Array<String>?
    */
    func extractEntityFromStringWithRegex(_regexPattern : String) -> Array<String>? {
        // if string count <= 0 then return nil
        if count(self) <= 0 {
            return nil
        }
        // create regex object
        var regex : Regex? = Regex(_pattern: _regexPattern)
        
        if let reg = regex {
            var result : Array<String> = [String]()
            if let matchedStrings = reg.extractEntityFromString(self) {
                return matchedStrings
            }
            
        }
        
        return nil
    }
    
    /*!
    * @discussion remove character from string by using regex
    * @param _regexPattern : String regex Pattern
    * @return String?
    */
    func removeCharactersWithRegex(_regexPattern : String) -> String? {
        // if string count <= 0 then return nil
        if count(self) <= 0 {
            return nil
        }
        // crete regecx object
        var regex : Regex? = Regex(_pattern: _regexPattern)
        
        if let reg = regex {
            return reg.removeSpecialCharacter(self)
        }
        
        return nil

    }
    
    

}