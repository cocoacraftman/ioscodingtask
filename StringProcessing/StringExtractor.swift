//
//  StringExtractor.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import Foundation

protocol StringExtractorDelegate {
    // Called when extracting start
    func didStartExtract()
    // Called when extracting finished with result
    func didFinishExtract(result : NSString)
    // Called when extracting got erro
    func didErrorWhileExtract(error : NSError)
}
class StringExtractor {
    // delegate
    var delegate : StringExtractorDelegate?
    // String to extracted
    let string : String
    // all user name are mentioned
    var mentionedUser : Array<String>?
    // all emotion string in string
    var emotions : Array<String>?
    // all url string
    var urls : Array<String>?
    // array of URLEntity (include url + title)
    var urlEntities : Array<URLEntity>?
    
    init(_string : String) {
        self.string = _string
    }
    
    /*!
    * @discussion : scan mention, emoticon and url from string -> convert to JSON
    * @param
    * @return
    */
    func extractedEntities()  {
        self.mentionedUser = self.scanForMetionedUsers()
        self.emotions = self.scanForEmoticons()
        self.urlEntities = self.scanForURLs()
        self.convertToJSON()
        
    }
    /*!
    * @discussion : convert dictionary to JSOn
    * @param
    * @return
    */
    private func convertToJSON()  {
        // create root Dict
        var rootDict : NSMutableDictionary = NSMutableDictionary()
        // create mention dictionary
        if let _mentionedUser = self.mentionedUser {
            rootDict.setObject(_mentionedUser, forKey: "mentions")
        }
        // create emotion dictionary
        if let _emotions = self.emotions {
            rootDict.setObject(_emotions, forKey: "emoticons")
        }
        
        // create url entities dictionry
        if let _urlEntities = self.urlEntities {
            var urlObjects : Array<NSMutableDictionary> = [NSMutableDictionary]()
            for entity in _urlEntities {
                var entityDict : NSMutableDictionary = NSMutableDictionary()
                entityDict.setValue(entity.url, forKey: "url")
                entityDict.setValue(entity.title, forKey: "title")
                urlObjects.append(entityDict)
            }
            rootDict.setObject(urlObjects, forKey: "links")
        }
        
        // call convert function
        self.convertDictionaryToJSONString(rootDict)
       
    }
    
    private func convertDictionaryToJSONString(rootDict : NSMutableDictionary) {
        // create JSON data
        var error : NSError?
        var jsonData = NSJSONSerialization.dataWithJSONObject(rootDict, options: NSJSONWritingOptions.PrettyPrinted, error: &error)
        
        if let err = error {
            // call delegate if error
            self.delegate?.didErrorWhileExtract(err)
        } else {
            if let jsonString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding) {
                // call delegate when success
                self.delegate?.didFinishExtract(jsonString)
            } else {
                // call delegate if can not get json
                var error : NSError = NSError(domain: "Can not parser object to JSON", code: 9874, userInfo: nil)
                self.delegate?.didErrorWhileExtract(error)
            }
        }
    }
    /*!
    * @discussion : scan mention from string -
    * @param
    * @return Array<String>?
    */
    func scanForMetionedUsers() -> Array<String>? {
        // get entity @username from string
        if let mentionStrings =  self.string.extractEntityFromStringWithRegex(RegexDefine.REGEX.MENTION_USER) {
            // remove duplicate mention name
            var uniqueMentionStrings = self.removeDuplicateMention(mentionStrings)
            // return list of usernames
            return self.removeSpecialCharacterWithRegex(RegexDefine.REGEX.REMOVE_AT_SIGH_CHARACTER, array: uniqueMentionStrings)
        } else {
            return nil
        }
    }
    /*!
    * @discussion : scan emoticon from string -
    * @param
    * @return Array<String>?
    */
    func scanForEmoticons() -> Array<String>?{
        // get emotion entity (emotion)
        if let emotionStrings = self.string.extractEntityFromStringWithRegex(RegexDefine.REGEX.EMOTICON) {
            // return list of emotion
           return self.removeSpecialCharacterWithRegex(RegexDefine.REGEX.REMOVE_PARENTHESES_CHARACTER, array: emotionStrings)

        } else {
            return nil
        }
    }
    /*!
    * @discussion : remove duplicate element in an array
    * @param
    * @return Generic Array
    */
    private func removeDuplicateMention<S: SequenceType, E: Hashable where E==S.Generator.Element>(source: S) -> [E] {
        var seen: [E:Bool] = [:]
        return filter(source) { seen.updateValue(true, forKey: $0) == nil }
    }
    
    /*!
    * @discussion : Remove special character with regex
    * @param regexPattern : String, array : Array<String>
    * @return Array<String>?
    */
    private func removeSpecialCharacterWithRegex(regexPattern : String,array : Array<String>)-> Array<String> {
        // create result array
        var resultArray : Array<String> = [String]()
        for str in array {
            if let newStr = str.removeCharactersWithRegex(regexPattern) {
                resultArray.append(newStr)
            } else {
                // call delegate if can not get new character
                var error : NSError = NSError(domain: "Can not remove special character", code: 9876, userInfo: nil)
                self.delegate?.didErrorWhileExtract(error)
            }
        }
        return resultArray
    }
    
    /*!
    * @discussion : scan url from string -
    * @param
    * @return Array<URLEntity>?
    */
    func scanForURLs()-> Array<URLEntity>? {
        // get list of url in string
        if let urls = self.string.extractEntityFromStringWithRegex(RegexDefine.REGEX.LINKs){
            return self.getURLEntities(urls)
        } else {
            return nil
        }
    }
    
    /*!
    * @discussion : get title from url
    * @param urls : Array<String>
    * @return Array<URLEntity>
    */
    private func getURLEntities(urls : Array<String>) -> Array<URLEntity> {
        
        var entities : Array<URLEntity> = [URLEntity]()
        
        for url in urls {
            // create new URLEntity
            var entity : URLEntity = URLEntity()
            entity.url = url
            if let requestURL = NSURL(string: url) {
                
                var error : NSError?
                // send request to request url content
                var urlStr = NSString(contentsOfURL: requestURL, encoding: NSUTF8StringEncoding , error: &error)
                if let err = error {
                    // if error -> call delegate
                    self.delegate?.didErrorWhileExtract(err)
                } else {
                    
                    if let string = urlStr {
                        // extract title by regex
                        if let regex = Regex(_pattern: RegexDefine.REGEX.TITLE_WEB) {
                            if let title = regex.getFirstEntityFromString(string as String) {
                                // remove <title></title> tag
                                entity.title = title.removeCharactersWithRegex(RegexDefine.REGEX.REMOVE_TITLE_TAG)
                            }
                        }
                        
                    } else {
                        // if can not get content ->? call delegate
                        var error : NSError = NSError(domain: "Can not get content from link", code: 9875, userInfo: nil)
                        self.delegate?.didErrorWhileExtract(error)

                    }
                }
            }
            entities.append(entity)
        }
        return entities
    }
   

}