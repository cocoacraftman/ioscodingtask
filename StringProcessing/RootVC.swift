//
//  ViewController.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import UIKit

class RootVC: UIViewController, UITableViewDelegate, UITableViewDataSource, StringExtractorDelegate {
    // indicate which testcase is selected
    var selectedIndex : Int = -1
    // progress hud when extracting too long
    var HUD : MBProgressHUD!
    // test case data
    let testCases : [String] = ["@chris you around?",
                                "Good morning! (megusta) (coffee)",
                                "Olympics are starting soon; http://nbcolympics.com/",
                                "@bob @john (success) such a cool feature; http://nbcolympics.com/ "
                                ]
    struct constants {
        static let cellIdenfitier : String = "RootVCCell"
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpViews()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : private functions
    private func setUpViews() {
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: RootVC.constants.cellIdenfitier)
    }
    
    //MARK: UITableViewDelegate & DataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testCases.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(RootVC.constants.cellIdenfitier, forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel!.text = String(format: "testCase %d", indexPath.row + 1)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex = indexPath.row
        var testCaseString : String = testCases[indexPath.row]
        //start extract
        var extractor : StringExtractor = StringExtractor(_string: testCaseString)
        extractor.delegate = self
        extractor.extractedEntities()
    }
    
    //MARK : StringExtractorDelegate
    func didStartExtract() {
        if HUD == nil {
            HUD = MBProgressHUD(forView: self.navigationController!.view)
            HUD.labelText = "Extracting..."
            HUD.dimBackground = true
        }
        self.navigationController!.view.addSubview(HUD)
        HUD.show(true)
    }
    
    func didErrorWhileExtract(error: NSError) {
        if HUD != nil {
            HUD.hide(true)
        }
        println(error)
    }
    
    func didFinishExtract(result: NSString) {
        if HUD != nil {
            HUD.hide(true)
        }
        println(result)
        // push result view controller
        var storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        var resultVC : ResultVC = storyBoard.instantiateViewControllerWithIdentifier("ResultVC") as! ResultVC
        resultVC.testCase = testCases[self.selectedIndex]
        resultVC.result = result as String
        self.navigationController!.pushViewController(resultVC, animated: true)
        
    }
    
    //MARK : prepare for segue
   


}

