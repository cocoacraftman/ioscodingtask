//
//  URLEntity.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import Foundation

class URLEntity {
    var url : String!
    var title : String?
}