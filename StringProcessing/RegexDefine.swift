//
//  RegexDefine.swift
//  StringProcessing
//
//  Created by Võ Long on 7/5/15.
//  Copyright (c) 2015 long.indie. All rights reserved.
//

import Foundation
class RegexDefine {
    struct REGEX {
        static let MENTION_USER : String = "(?<!\\w\\p{L})@([\\w\\p{L}]{1,225})"
        static let EMOTICON : String = "\\(([a-zA-Z0-9]{1,15})\\)"
        static let LINKs : String = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        static let REMOVE_AT_SIGH_CHARACTER : String = "@|＠"
        static let REMOVE_PARENTHESES_CHARACTER : String = "\\(|\\)"
        static let REMOVE_TITLE_TAG : String = "<title>|</title>"
        static let TITLE_WEB : String = "<title[^>]*>(.*?)</title>"
    }
}